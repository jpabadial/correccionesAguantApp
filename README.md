creadores: Juan Pablo Abadia
           Nicolas Reeyes
fecha: abril/24/2017

aguantApp es una aplicacion web Movil geosocial que permite a los usuarios comunicarse con
otras personas con base en sus preferencias(rango de edad, distancia y genero)
para charlar por medio de un chat en tiempo real, esta aplicacion permite
concretar citas o encuentros hot. aguantApp fue creada en abril del 2017 por
Juan Pablo Abadia y Nicolas Reyes.

FUNCIONALIDADES REQUERIDAS POR EL PROFESOR:
1.versionamiento ✔
2.branches -> Nicolas, jp, Master ✔
3.branches Nicolas y jp unificadas a la Master ✔
4.diseño flat ✔
5.login ✔
6.registro ✔
7.sección Home, donde se encuentran las personas que no se han calificado ✔
8.configuracion donde se modifican datos del Usuario regustrado en ese momento ✔
9.Perfil de información, donde aprece la informacion adicional como distancia, nombre de las personas del home ✔
10.Perfil propio, corresponde a cada usuario registrado ✔
11.chat para conversar con los futuros matches ✔
12.La app cuanta con un menu superiro donde esta el perfil de la persona logeada, donde estan los futuros Matches y un repositorio de los chats creados en caso de haber un matches ✔
13.lazy load ✔
14.Registro de usuarios con la api de google, toma lat y log por defecto ✔
15.Seleccion de genero por parte de los usuarios registrados para encontrar futuros Matches ✔
16.las personas del Home, salen de acuerdo a loos filtros de genero, edad y distancia ✔
17.usuario que no "aguantan" ya no salen nuevamente, y los que "aguantan" salen en la seccion del chat ✔
18.El usuario en el chat podra decir que ya no aguanta ✔
19.notificacion al moento de existir un Match ✔
20.Los usuarios pueden subir n fotos y hacer cualquiera foto de perfil ✔
21.Los usuarios podran borrar cualquier imagen de su perfil y esta se eliminara del servido y de la DB ✔
22.
23.Los usuarios que tienen conpatibilidad, es decir, que ambos se "aguantan" podran chatear en tiempo real ✔
24.USO DE LAZY LOAD ✔


COMO FUNCIONA:

los usuario deberan registrar sus datos basico como:
✔correo electronico
✔nombre de usuario
✔contraseña
✔edad
✔tipo de genero

una vez registrados estos datos, el usuario podra realizar el login en la
aplicacion, la cual solo requiere de:
✔nombre de usuario
✔contraseña
cuando el usuario de sobre el boton "Login" pasara a una ventana de carga, la cual
redirecciona a la persona a la pantalla de PERFIL, la cual consta de tres secciones:
✔INFORMACION PERSONAL
✔FOTOS
✔CONFIGURACION
en la seccion de informacion personal, el usuario podra modificar tres campos, edad, sobre mi y estado,
en el siguiente bloque de fotos el suaurio podra cargar fotos para tenerlas en su perfil, al hacer
click sobre una de esas fotos podra hacer una de esas fotos como foto de perfil y tambien podra eliminarlas.
en la sigueinte seccion de configuracion el suario podra filtrar a otras peronas por
distancia de busqueda, por edades y por tipo de genero.

Donde se encuentra el logo de la aplicaion, se encuentras las peronas que son filtradas
por el suaurio logeado en ese moneto, en esta seccion se encuentra informacion basica y en
el menu de la parte inferior el usuario podra ver la infromacion personl, podra ver las
fotos y ademas cuenta con dos botones, los cuales sirven para darle "aguanta" y "no aguanta";
una vez exista un Match entre dos suaurios(usuaio logeado y otro usuario), el usuario
sera notificado de que existe un match y podra acceder a la seccion de mensajes, donde tendra un
registro de todos los matches que tiene, y con los cuales podra hablar.

creditos****
