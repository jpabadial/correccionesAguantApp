<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="./css/estilo.css" rel="stylesheet" type="text/css">

  </script>
  <title>AguantApp</title>
</head>
<body>
  <a href="profile.html">
    <div class="zonapersona">
      <div class="cajapersona">
        <div class="imagenpersona">
          <img src="./img/lamejor.jpg" alt="">
        </div>
        <div class="info">
          <div class="titulo">Emma, 26</div>
          <div class="detalle">Amor de tu vida</div>
        </div>
      </div>
    </div>
  </a>
  <div class="corazones">
    <div class="noaguanta">
      <img src="./icons/brokenHeart.svg" alt="">
    </div>
    <div class="aguanta">
      <img src="./icons/fixedHeart.svg" alt="">
    </div>
  </div>
  <footer>
    <div class="menu">
      <div class="seclogo">
        <a href="home.html"><img src="./icons/flameHeart_vector.svg" alt="img"></a>
      </div>
      <div class="sec">
        <a href="ownProfile.html"><img src="./icons/iusuario.svg" alt="img"></a>
      </div>
      <div class="sec">
        <a href="config.html"><img src="./icons/engranaje.svg" alt="img"></a>
      </div>
      <div class="sec">
        <a href="buzon.html"><img src="./icons/msj.svg" alt="img"></a>
      </div>
    </div>
  </footer>
  <script src="./js/jquery-3.1.0.min.js"></script>
  <script src="./js/ux.js"></script>
  <script src="./js/checkMatch.js"></script>
</body>
</html>
