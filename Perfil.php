<!DOCTYPE html>
<html lang="es" class="no-js">

<?php include("./controladores/Foto_Controller.php"); ?>

<head>
    <meta charset="utf-8">
    <title>Perfil</title>
    <link rel="stylesheet" href="./css/subirStyle.css">
    <link rel="stylesheet" href="./css/perfilStyle.css">
    <link rel="stylesheet" href="./css/fontello.css">
    <link rel="stylesheet" href="./css/animate.css">
    <link rel="stylesheet" href="./sources/jquery.mobile-1.4.5/jquery.mobile-1.4.5.css">
    <script type="text/javascript" src="./sources/jQuery/jquery-3.2.0.min.js"></script>
    <script type="text/javascript" src="./sources/jQuery/jquery.lazyload.js"></script>
    <script type="text/javascript" src="./sources/push.min.js"></script>
    <script src="./node_modules/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./node_modules/sweetalert/dist/sweetalert.css">
    <!-- <script type="text/javascript" src="./sources/jquery.mobile-1.4.5/jquery.mobile-1.4.5.js"></script> -->

</head>

<body>
    <div class="wrapper">
        <header>
            <nav>
                <div class="nav-profile" id="perfilConfig">
                    <h1 class="icon-me"></h1>
                </div>
                <div class="nav-logo" id="inicio">
                    <img src="./recursos/imagenes/logo.png" alt="">
                </div>
                <div class="nav-messege">
                    <h1 class="icon-chat" id="buzoni"></h1>
                </div>

            </nav>
        </header>

    </div>


    <div class="perfil-persona-to">


        <div class="todo-form">
            <form class="editme">
                <div class="nombre-input">
                    <div class="icon-nombre">
                        <h1 class="icon-users"></h1>
                    </div>
                    <div class="nombre-input-input">
                        <p>Edad:</p>

                        <input type="text" name="edad">
                        <br>
                    </div>
                </div>

                <div class="sobre-me-input">
                    <div class="icon-sobre-me">
                        <h1 class="icon-me"></h1>
                    </div>
                    <div class="sobre-me-input-input">
                        <p>Sobre mi:</p>
                        <input type="text" name="descripcion">
                        <br>
                    </div>
                </div>


                <div class="estado-input">
                    <div class="icon-estadoP">
                        <h1 class="icon-estado"></h1>
                    </div>
                    <div class="estado-input-input">
                        <p>Estado:</p>
                        <input type="text" id="lastname" name="estado">
                        <br>
                    </div>
                </div>

                <div class="button-guardar">
                    <a href="opciones-menu-perfil">
                        <button type="submit" class="button button2"  id="guardarDat">Guardar</button>
                    </a>
                </div>
            </form>
        </div>


    </div>


    <!--  -->
        <div class="cargaFoto">
            <div class="todo-form">
                  <div class="contenedorEmergente">
                    <div class="seleccion">
                      <form accept-charset="utf-8" method="POST" id="enviarimagenes" enctype="multipart/form-data">
                        <input type="file" name="imagen" id="file" class="inputfile inputfile-2" data-multiple-caption="{count} files selected" multiple />
                        <label for="file"><figure><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg></figure> <span>Cargar foto&hellip;</span></label>
                        <input type="submit" class="btna" name="button" width="100px" height="100px"><!--buton  -->
                      </form>
                    </div>
                  </div>
            </div>
        </div>
        <script >
        $("#enviarimagenes").submit(function(e) {
          e.preventDefault();
          var formData = new FormData(document.getElementById("enviarimagenes"));
          $.ajax({
            type: "POST",
            dataType: "HTML",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            url: "http://localhost/aguantApp/?controller=Foto_Controller&method=saveImagen"
          }).done(function(r) {
            console.log(r);
            swal({title:"Carga Exitosa", text:"Foto cargada correctamente", type:"success",closeOnConfirm: false},
                function(){

                   location.reload();
                });
          });
        });

        </script>




        <!--  -->



    <!-- <div class="configura-checkBox">
    <h2>PREFERENCIA: </h2>


</div> -->
<?php $usuarios= Usuarios_Controller::cargarPerfil(); ?>
<?php $fotos= Foto_Controller::cargarFotos(); ?>

<?php  foreach($usuarios as $usuario):?>
    <div class="opciones-menu-perfil">
        <div class="lateral-perfil">

            <div class="foto-perfil">

                <div class="contenido-perfil">
                    <div class="imagen-perfil">
                        <!-- <a href=""> -->
                        <div class="circular">

                        <img src=<?php echo '"'.$usuario["imgProfile"].'"'; ?> alt=""></a>

                      </div>
                    </div>
                    <div class="nombre-perfil">
                        <h3><?php echo $usuario["edad"]; ?> \ </h3>
                        <h3 > <?php echo $usuario["username"]; ?></h3>
                    </div>
                </div>
            </div>



            <div class="superiorTo">
                <div class="bloque-perfil" id="infometo">
                    <h1 class="icon-info-outline"></h1>
                </div>
                <div class="bloque-perfil" id="albumto">
                    <h1 class="icon-picture"></h1>
                </div>
                <div class="bloque-perfil" id="liketo">
                    <h1 class="icon-cog"></h1>
                </div>


            </div>

            <div class="inferiorTo">
                <div class="inferior-bloque-to">
                    <div class="title-bloque-to">
                        <div class="infoPersonal">
                            <h2>INFO PERSONAL</h2>
                        </div>
                        <div class="icon-infoPersonal">
                            <h1 class="icon-user-add" id="location"></h1>
                        </div>

                    </div>
                    <div class="about-bloque-to">
                        <div class="about-icon-bloque-to">
                            <h1 class="icon-me"></h1>
                        </div>
                        <div class="about-info-bloque-to">
                            <div class="title-info-about-to">
                                <h3>SOBRE MI</h3>
                            </div>
                            <div class="text-info-about-to">
                                <p><?php echo $usuario["descripcion"]; ?></p>
                            </div>

                        </div>

                    </div>
                    <div class="location-bloque">
                        <div class="location-icon">
                            <h1 class="icon-location"></h1>
                        </div>
                        <div class="location-information">
                            <div class="information-title">
                                <h3>UBICACION</h3>
                            </div>
                            <div class="information-text">
                                <p><?php echo $usuario["ciudad"]; ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="estado-bloque">
                        <div class="estado-icon">
                            <h1 class="icon-estado"></h1>
                        </div>
                        <div class="estado-information">
                            <div class="estado-title-information">
                                <h3>ESTADO</h3>
                            </div>
                            <div class="estado-text-information">
                                <p><?php echo $usuario["estado"]; ?></p>
                            </div>
                        </div>

                    </div>

                </div>


                <div class="inferior-fotos">
                    <div class="title-bloque-to">
                        <div class="infoPersonal">
                            <h2>GALERIA</h2>
                        </div>
                        <div class="icon-infoPersonal2  enviarimagenes">

                            <h1 class="icon-picture" id="location22" style="text-decoration:none" type="submit" onclick="actulizar();"></h1>
                        </div>

                    </div>
                    <div class="convoyer">
                        <div class="elements">
                            <?php foreach($fotos as $foto): ?>
                                <div class="element itemClass" >
                                    <img data-original= <?php echo '"'.'./'.$foto["archivo"].'"'; ?> alt="" class="lazyLoad" id=<?php echo $foto["id"]; ?>>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>


                <div class="inferior-configura">
                    <div class="title-bloque-to">
                        <div class="infoPersonal">
                            <h2>CONFIGURACÍON</h2>
                        </div>
                        <div class="icon-infoPersonal2">

                            <h1 class="icon-floppy" id="location2" style="text-decoration:none" type="submit" ></h1>
                        </div>

                    </div>
                    <form class="confiRangos" action="index.html" method="post">

                        <div class="configura-jSlider1">
                            <div class='info1'>
                                <div class="icon-info1">
                                    <h1 class="icon-compass" id="icono"></h1>
                                </div>

                                <div class="SliderYtitle1">
                                    <div class="title-km">
                                        <h2>DISTACIA DE BÚSQUEDA : </h2>
                                    </div>

                                    <div class="input-jslider1">
                                        <input type="text" id="valueSlider1" value="0" name="radio"/>
                                    </div>
                                    <div class='slider1'>
                                        <div class='emptyprogress1'></div>
                                        <div class='progress1'></div>
                                        <div class='indicator1'></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="configura-jSlider2">
                            <div class='info2'>
                                <div class="icon-info2">
                                    <h1 class="icon-users" id="icono"></h1>
                                </div>
                                <div class="SliderYtitle2">
                                    <h2>MOSTRAR EDADES : </h2>
                                    <input type="text" id="valueSlider2" value="0" name="rangoEdad"/>

                                    <div class='slider2'>
                                        <div class='emptyprogress2'></div>
                                        <div class='progress2'></div>
                                        <div class='indicator2'></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="configura-checkBox">
                            <div class='infoCheck'>
                                <div class="icon-check">
                                    <h1 class="icon-transgender " id="icono"></h1>
                                </div>
                                <div class= "contenido-checks">
                                    <h2>MUÉSTRAME : </h2>

                                    <div class="contenidos-input">
                                        <div class="input1">
                                            <input type="radio" id="radio01" name="radioh" value="1"/>
                                            <label for="radio01"><span id="manId"></span>Hombre</label>
                                        </div>
                                        <div class="input2">
                                            <input type="radio" id="radio02" name="radiom" value="0"/>
                                            <label for="radio02"><span id="womanId"></span>Mujer</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

    </div>
<?php endforeach; ?>
</div>

<script src="./js/ajax.js"></script>
<script src="./js/ux-perfil.js"></script>
<script src="js/custom-file-input.js"></script>
<script>
function actulizar(){
  console.log("sisisiisisisi");
  setTimeout(function() {
    location.reload();
    }, 300);
}
</script>

</body>

</html>
