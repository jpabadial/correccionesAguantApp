-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-05-2017 a las 02:12:07
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `aguantapp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aguanta`
--

CREATE TABLE `aguanta` (
  `idUsuario` int(11) NOT NULL,
  `idAguanta` int(11) NOT NULL,
  `aguanta` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chat`
--

CREATE TABLE `chat` (
  `idUno` int(11) NOT NULL,
  `idOtro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foto`
--

CREATE TABLE `foto` (
  `id` int(11) NOT NULL,
  `usuario` int(11) NOT NULL,
  `archivo` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `foto`
--

INSERT INTO `foto` (`id`, `usuario`, `archivo`) VALUES
(170, 76, 'img/jpabadial2.jpg'),
(171, 76, 'img/jpabadial.jpg'),
(172, 76, 'img/jpabadial3.jpg'),
(173, 77, 'img/daniela.png'),
(174, 77, 'img/daniela2.jpg'),
(175, 77, 'img/daniela3.jpg'),
(176, 78, 'img/aleja.jpg'),
(177, 78, 'img/aleja2.jpg'),
(178, 78, 'img/aleja3.jpg'),
(179, 83, 'img/ronaldinho.jpg'),
(180, 83, 'img/ronaldinho2.jpg'),
(181, 83, 'img/ronaldinho3.jpg'),
(182, 84, 'img/carolina.jpg'),
(183, 84, 'img/carolina2.jpg'),
(184, 84, 'img/carolina3.jpg'),
(185, 85, 'img/danii.png'),
(186, 85, 'img/danii2.jpg'),
(187, 85, 'img/danii3.png'),
(188, 86, 'img/pao.jpg'),
(189, 86, 'img/pao2.jpg'),
(190, 86, 'img/pao3.jpg'),
(191, 81, 'img/isa.jpg'),
(192, 81, 'img/isa2.jpg'),
(193, 81, 'img/isa3.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `interes`
--

CREATE TABLE `interes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `intereses_usuario`
--

CREATE TABLE `intereses_usuario` (
  `idUsuario` int(11) NOT NULL,
  `idInteres` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `intereses_usuario`
--

INSERT INTO `intereses_usuario` (`idUsuario`, `idInteres`) VALUES
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensaje`
--

CREATE TABLE `mensaje` (
  `id` int(11) NOT NULL,
  `owner` int(11) NOT NULL,
  `texto` longtext NOT NULL,
  `idChatUno` int(11) NOT NULL,
  `idChatOtro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `genero` int(11) NOT NULL,
  `lat` varchar(45) NOT NULL,
  `lon` varchar(45) NOT NULL,
  `radio` float NOT NULL,
  `estado` varchar(45) NOT NULL,
  `descripcion` varchar(150) NOT NULL,
  `ciudad` varchar(45) NOT NULL,
  `edad` decimal(10,0) NOT NULL,
  `rangoEdad` varchar(45) NOT NULL,
  `imgProfile` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `username`, `password`, `email`, `genero`, `lat`, `lon`, `radio`, `estado`, `descripcion`, `ciudad`, `edad`, `rangoEdad`, `imgProfile`) VALUES
(76, 'jpabadial', 'jpabadial', 'jpabadial@gmail.com', 1, '3.4096724999999997', '-76.5466457', 100, 'Solter@', 'Sin descripcion', 'Cali', '21', '30', '/aguantApp/img/jpabadial2.jpg'),
(77, 'daniela', 'daniela', 'daniela@gmail.com', 2, '3.4096802999999998', '-76.54664129999999', 100, 'Solter@', 'Sin descripcion', 'Cali', '20', '30', '/aguantApp/img/daniela2.jpg'),
(78, 'aleja', 'aleja', 'alejandra@hotmail.com', 2, '3.4097597', '-76.54667649999999', 100, 'Solter@', 'Sin descripcion', 'Cali', '19', '30', '/aguantApp/img/aleja3.jpg'),
(79, 'camilo', 'camilo', 'camilo@hotmail.es', 1, '3.4097594', '-76.5466754', 100, 'Solter@', 'Sin descripcion', 'Cali', '34', '30', ''),
(80, 'valen', 'valen', 'valentina@gmail.com', 2, '3.4097716', '-76.5466789', 100, 'Solter@', 'Sin descripcion', 'Cali', '22', '30', ''),
(81, 'isa', 'isa', 'isa@hotmail.com', 2, '3.4096832999999998', '-76.5466785', 124, 'soltera', 'periodista, modelo y presentadora de televisiÃ³n colombiana, reconocida por haber presentado los programas Muy Buenos DÃ­as, Estilo RCN ', 'Cali', '26', '33', '/aguantApp/img/isa.jpg'),
(82, 'juan', 'juan', 'juan@gmail.com', 1, '3.4096838999999997', '-76.5466819', 100, 'Solter@', 'Sin descripcion', 'Cali', '21', '30', ''),
(83, 'ronaldinho', 'ronaldinho', 'ronaldhino@hotmail.es', 1, '3.4096789999999997', '-76.54667909999999', 100, 'soltero', 'Ronaldo de Assis Moreira, conocido deportivamente como Ronaldinho, es un futbolista brasileÃ±o nacionalizado espaÃ±ol. Actualmente es embajador del FC', 'Cali', '33', '30', '/aguantApp/img/ronaldinho.jpg'),
(84, 'carolina', 'carolina', 'carolina@hotmail.es', 2, '3.4097525', '-76.5466728', 100, 'Solter@', 'Sin descripcion', 'Cali', '26', '30', '/aguantApp/img/carolina2.jpg'),
(85, 'danii', 'danii', 'danii@hotmail.com', 2, '3.4096786999999997', '-76.5466454', 100, 'Solter@', 'Sin descripcion', 'Cali', '21', '30', '/aguantApp/img/danii3.png'),
(86, 'pao', 'pao', 'paola@gmail.com', 2, '3.4097739', '-76.5466514', 100, 'casada', ' modelo, empresaria y presentadora colombiana.', 'Cali', '21', '30', '/aguantApp/img/pao3.jpg'),
(87, 'lu', 'lu', 'luisa@hotmail.es', 2, '3.4097739', '-76.5466514', 100, 'Solter@', 'Sin descripcion', 'Cali', '20', '30', ''),
(88, 'ju', 'ju', 'ju@h.co', 1, '3.4433746999999997', '-76.50899489999999', 100, 'Solter@', 'Sin descripcion', 'Cali', '18', '30', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aguanta`
--
ALTER TABLE `aguanta`
  ADD PRIMARY KEY (`idUsuario`,`idAguanta`);

--
-- Indices de la tabla `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`idUno`,`idOtro`);

--
-- Indices de la tabla `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `interes`
--
ALTER TABLE `interes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `intereses_usuario`
--
ALTER TABLE `intereses_usuario`
  ADD PRIMARY KEY (`idUsuario`,`idInteres`);

--
-- Indices de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `foto`
--
ALTER TABLE `foto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=194;
--
-- AUTO_INCREMENT de la tabla `genero`
--
ALTER TABLE `genero`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `interes`
--
ALTER TABLE `interes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
